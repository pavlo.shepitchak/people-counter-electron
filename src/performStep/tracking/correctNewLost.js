// @flow
import eventEmitter, { event } from '../eventEmiter';
import getState from './getState';
import { releaseId } from './idGenerator';
import isObjectContainsAnother from './isObjectContainsAnother';



export default (
  prevFrame: Array<TrackedObject>,
  nextFrame: Array<TrackedObject>
): Array<TrackedObject> => {
  const newOnes = nextFrame.filter(
    item => !prevFrame.find(prevItem => prevItem.id === item.id)
  );
  const lostOnes = prevFrame.filter(
    item => !nextFrame.find(nextItem => nextItem.id === item.id)
  );
  lostOnes.forEach(item => releaseId(item.id));

  const corrected = [];
  //manage splited objects
  lostOnes.forEach(lostOne => {
    const children = newOnes.filter(child =>
      isObjectContainsAnother(lostOne, child)
    );
    if (children.length === 1 && children[0].id === lostOne.id) return;
    children.forEach(child => {
      const state = getState(lostOne.center, child.center);
      const clone = { ...child, state, timer: lostOne.timer };
      clone.timer = lostOne.timer;
      if (state !== 'NONE' && !clone.timer) {
        eventEmitter.emit(event.COUNTER_UPDATE, state);
        clone.timer = 10;
      }
      corrected.push(clone);
    });
  });

  ///manage joined objects
  newOnes.forEach(newOne => {
    const children = lostOnes.filter(child =>
      isObjectContainsAnother(newOne, child)
    );
    if (children.length === 1 && children[0].id === newOne.id) return;
    const clone = {
      ...newOne,
      timer: 3 // Math.max(...children.map(({ timer }) => timer))
    };
    children.forEach(child => {
      const state = getState(child.center, newOne.center);
      if (state !== 'NONE' && !child.timer) {
        eventEmitter.emit(event.COUNTER_UPDATE, state);
        clone.timer = 10;
        clone.state = state;
      }
    });
    corrected.push(clone);
  });
  return nextFrame.map(item => corrected.find(c => c.id === item.id) || item);
};
