// @flow
import config from '../../config';

export default (prevCenter: Center, nextCenter: Center): State => {
  const { barrier } = config;
  const [[yp], [yn]] = [prevCenter, nextCenter];
  if (yn === barrier || yp === barrier) {
    console.log('holding');
  }
  if (yn > barrier && yp < barrier) {
    return 'DOWN';
  } else if (yn < barrier && yp > barrier) {
    return 'UP';
  }
  return 'NONE';
};
