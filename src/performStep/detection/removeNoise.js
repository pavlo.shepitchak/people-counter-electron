// @flow
import morph from './morph';
import sharp from 'sharp';
import config from '../../config';
import { flatten } from 'lodash';

const brush0 = morph.generateStructureElement(1);

export default async (image: Sharp): Promise<Sharp> =>
  await morphOpen(await morphClose(image));

export const morphClose = async (image: Sharp): Promise<Sharp> => {
  const brush = morph.generateStructureElement(config.morphBrushSize);
  return await applyToMatrix(image, matrix => morph.close(matrix, brush));
};

export const morphOpen = async (image: Sharp): Promise<Sharp> => {
  const brush = morph.generateStructureElement(config.morphBrushSize);
  return await applyToMatrix(image, matrix => {
    return morph.open(morph.dilate(matrix, brush0), brush);
    //return morph.dilate(morph.dilate(matrix, brush0), brush);
  });
};
const applyToMatrix = async (
  image: Sharp,
  func: (Array<number>) => Array<number>
) => {
  const buffer = await image
    .clone()
    .raw()
    .toBuffer();
  const { width, height } = await image.metadata();
  const input = listToMatrix(buffer, width);
  const pixels = func(input);
  return await sharp(Buffer.from(flatten((pixels: any))), {
    raw: { width: width, height: height, channels: 1 }
  })
    .png()
    .greyscale()
    .threshold(1);
};
function listToMatrix(list: Buffer, width = 320): Array<number> {
  const height = list.length / width;
  const indexes = Object.keys((Array(height): any).fill(0));
  return indexes.map(
    index => (list.subarray(index * width, (index + 1) * width): any)
  );
}
