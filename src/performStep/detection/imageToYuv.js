// @flow
import { chunk, range } from 'lodash';

export default async (image: Sharp): Promise<Array<Array<number>>> => {
  //console.time('all');
  //console.time('toBuffer');
  const buffer = await image.raw().toBuffer();
  //console.timeEnd('toBuffer');
  //console.time('chunk');
  const pixels = chunk((buffer: any), 3);
  //console.timeEnd('chunk');
  //console.time('pixelToYuv');
  const fib = n => {
    return n;
  };
  const result = pixels.map(pixelToYuv);
  //console.timeEnd('pixelToYuv');
  //console.timeEnd('all');
  return result;
};
const pixelToYuv = ([r, g, b]: Array<number>): Array<number> => [
  0.299 * r + 0.587 * g + 0.114 * b,
  -0.14713 * r - 0.28886 * g + 0.436 * b + 12,
  0.615 * r - 0.51499 * g - 0.10001 * b + 128
];
