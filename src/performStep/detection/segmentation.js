// @flow
import sharp from 'sharp';
import config from '../../config';
//import { range, flatten, chunk } from 'lodash';
const { range, flatten, chunk } = require('lodash');
type MatrixNumeric = Array<Array<number>>;

const f = (a, b) => [].concat(...a.map(d => b.map(e => [].concat(d, e))));
const multiplySets = (a, b, ...c) => (b ? multiplySets(f(a, b), ...c) : a);
const inRange = ([y, x]: Pixel, array: MatrixNumeric): boolean =>
  y >= 0 && y < array.length && x >= 0 && x < array[0].length;

const fill = (array: MatrixNumeric, pixel: Pixel, number: number): void => {
  const getNearbyPixels = ([y, x]: Pixel): Array<Pixel> => [
    [y, x - 1],
    [y, x + 1],
    [y - 1, x],
    [y + 1, x]
  ];
  let nearbyPixels = getNearbyPixels(pixel);
  while (nearbyPixels.length) {
    nearbyPixels = nearbyPixels.map(pixel => {
      const [y, x] = pixel;
      if (!inRange(pixel, array) || array[y][x] !== 1) return [];
      array[y][x] = number;
      return getNearbyPixels(pixel);
    });
    nearbyPixels = flatten(nearbyPixels);
  }
};

function* findUndefinedPixel(
  array: MatrixNumeric
): Generator<Pixel, void, void> {
  for (let i of range(0, array.length)) {
    for (let j of range(0, array[0].length)) {
      if (array[i][j] === 1) {
        yield [i, j];
      }
    }
  }
}
function* indexGenerator(): Generator<number, void, void> {
  let i = 1;
  while (true) yield ++i;
}

const segment = (image: MatrixNumeric): Array<MatrixNumeric> => {

  const getIndex = indexGenerator();
  const result = image.map(i => [...i]);
  let lastIndex;
  const undefinedArea = findUndefinedPixel(result);

  for (let value of undefinedArea) {
    const { value: index } = getIndex.next();
    fill(result, value, (index: any));
    lastIndex = index;
  }

  const segmentIndexes = lastIndex ? range(2, lastIndex + 1) : [];
  return segmentIndexes.map(index =>
    result.map(row => row.map(p => (p === index ? 1 : 0)))
  );

};
const getBounds = image => {
  const verticalProjection = image.map(row => (row.some(i => i === 1) ? 1 : 0));
  const top = verticalProjection.indexOf(1);
  const bottom = verticalProjection.lastIndexOf(1);
  const left = Math.min(
    ...image.map(row => row.indexOf(1)).filter(i => i >= 0)
  );
  const right = Math.max(
    ...image.map(row => row.lastIndexOf(1)).filter(i => i >= 0)
  );
  return { top, bottom, left, right };
};
const getCenter = ({ top, bottom, left, right }) => [
  Math.round(top + (bottom - top) / 2),
  Math.round(left + (right - left) / 2)
];
const matrixToSharp = (matrix: MatrixNumeric): Sharp =>
  sharp(Buffer.from(flatten((matrix: any))), {
    raw: {
      width: matrix[0].length,
      height: matrix.length,
      channels: 1
    }
  })
    .png()
    .greyscale()
    .threshold(1);
const calculateWeight = (matrix: MatrixNumeric): number =>
  flatten(matrix).filter(i => i === 1).length;
const getSegmentData = (segment: MatrixNumeric): SegmentData => {
  const bounds = getBounds(segment);
  const center = getCenter(bounds);
  const mask = matrixToSharp(segment);
  const weight = calculateWeight(segment);
  const peopleNumber = Math.round(weight / config.humanSize);
  return { mask, center, bounds, weight, peopleNumber };
};
export default async (image: Sharp): Promise<Array<SegmentData>> => {
  const { width } = await image.metadata();
  const buffer = await image
    .clone()
    .raw()
    .toBuffer();
  const array: any = buffer.map(i => (i ? 1 : 0));
  const segments = segment(chunk(array, width));
  return segments.map(getSegmentData).filter( item => item.peopleNumber);
};
