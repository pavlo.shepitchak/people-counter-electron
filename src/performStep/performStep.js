// @flow
import detection from './detection/index';
import { updateBackground } from '../utils/index';
import { getTrackedObjects } from './tracking';
import counter from './counter';

let _background: ?Sharp = null;
type PerformStepResult = {
  frame: Sharp,
  background: Sharp,
  difference: Sharp,
  withoutNoise: Sharp,
  objects: Array<TrackedObject>,
  counterValue: { up: number, down: number }
};

export default async (
  nextFrame: Sharp,
  background?: Sharp
): Promise<PerformStepResult> => {
  _background = _background || background || nextFrame;
  const frameClone = nextFrame.clone();
  const { diff, withoutNoise, segments } = await detection(
    nextFrame,
    _background
  );
  _background = await updateBackground(_background, nextFrame, segments);
  const trackedObjects = getTrackedObjects(segments);
  return {
    frame: frameClone,
    background: _background,
    difference: diff,
    withoutNoise,
    objects: trackedObjects,
    counterValue: counter.getValue()
  };
};
