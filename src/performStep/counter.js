// @flow
class Counter {
  constructor() {
    this.value = { down: 0, up: 0 };
  }
  increaseCounterDown() {
    const { up, down } = this.value;
    this.value = { up, down: down + 1 };
  }
  increaseCounterUp() {
    const { up, down } = this.value;
    this.value = { up: up + 1, down };
  }
  getValue() {
    return this.value;
  }
}
export default new Counter();
