// @flow
import sharp from 'sharp';

export const joinHorizontally = (image1: Sharp, image2: Sharp) =>
  join(image1, image2, 'horizontal');
export const joinVertically = (image1: Sharp, image2: Sharp) =>
  join(image1, image2, 'vertical');
export const join4 = async (images: Array<Sharp>) => {
  const verticalParts = [
    await joinHorizontally(images[0], images[1]),
    await joinHorizontally(images[2], images[3])
  ];
  return joinVertically(...verticalParts);
};

const join = async (
  image1: Sharp,
  image2: Sharp,
  direction: 'vertical' | 'horizontal'
) => {
  const { west, east, north, south } = sharp.gravity;
  const viewport = await createViewPort(image1, image1 || image2, direction);
  const gravity = direction === 'horizontal' ? [west, east] : [north, south];
  let result = !image1
    ? viewport
    : viewport.overlayWith(await image1.toBuffer(), {
        gravity: gravity[0]
      });
  result = await recreateImage(result);
  result = !image2
    ? result
    : result.overlayWith(await image2.toBuffer(), {
        gravity: gravity[1]
      });
  return result;
};

const recreateImage = async (image: Sharp): Promise<Sharp> => {
  const buffer = await image.toBuffer();
  return sharp(buffer);
};

const createViewPort = async (
  image1: Sharp,
  image2: Sharp,
  direction: 'vertical' | 'horizontal'
) => {
  const { width: w1, height: h1 } = await image1.metadata();
  const { width: w2, height: h2 } = await image1.metadata();
  return await sharp(null, {
    create: {
      width: direction === 'horizontal' ? w1 + w2 : Math.max(w1, w2),
      height: direction === 'vertical' ? h1 + h2 : Math.max(h1, h2),
      channels: 3,
      background: { r: 255, g: 255, b: 255 }
    }
  }).png();
};
